<?php
/* 
    Plugin Name: Date Shortcode
    Description: Display The Current Day 
    Author: Babici Bojidar Alin
    Version: 1.0 
    */
	
 function displayTodaysDate( $atts )
    {
        return date(('m.d.y'));
    }

    add_shortcode( 'shortcodedate="m.d.y"', 'displayTodaysDate');
	
function displayTodaysDate2( $atts )
    {
        return date(('l, F j, Y'));
    }

    add_shortcode( 'shortcodedate="l, F j, Y"', 'displayTodaysDate2');
	
function displayTodaysDate3( $atts )
    {
        return date(('F j, Y'));
    }

    add_shortcode( 'shortcodedate="F j, Y"', 'displayTodaysDate3');
	
function displayTodaysDate4( $atts )
    {
        return date(('M j, Y'));
    }

    add_shortcode( 'shortcodedate="M j, Y"', 'displayTodaysDate4');
	
function displayTodaysDate5( $atts )
    {
        return date(('Y/M/D'));
    }

    add_shortcode( 'shortcodedate="Y/M/D"', 'displayTodaysDate5');
?>

